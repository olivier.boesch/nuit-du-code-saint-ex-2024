"""
Gestion et test d'un gamepad

Exemples de récupération des valeurs des boutons et des axes.

Olivier Boesch (c) 2024
"""

# import de la bibliothèque pyxel et on la renomme en "p"
import pyxel as p

# initialisation de la fenetre (200x120pixels, 30 images par seconde)
p.init(213,120, fps=30)

# affichage en plein écran
# p.fullscreen(True)

# ------------- variables
# texte des boutons appuyés
s = ""
# valeur de l'axe horizontal du stick de gauche
axis_leftx = 0  # entre -32768 et 32768
# valeur de l'axe vertical du stick de gauche
axis_lefty = 0  # entre -32768 et 32768
# bouton du stick gauche
button_left = False
# valeur de l'axe horizontal du stick de droite
axis_righty = 0  # entre -32768 et 32768
# valeur de l'axe vertical du stick de droite
axis_rightx = 0  # entre -32768 et 32768
# bouton du stick droit
button_right = False
# valeur de la gachette gauche
axis_lefttrig = 0  # entre 0 et 32768
# valeur de la gachette droite
axis_righttrig = 0  # entre 0 et 32768


def update():
    """Mise à jour des variables"""
    global s, axis_leftx, axis_lefty, axis_righty, axis_rightx, axis_lefttrig, axis_righttrig, button_left, button_right
    s = ""

    # récupération des boutons appuyés et ajout à la chaine s de leur nom
    if p.btn(p.GAMEPAD1_BUTTON_GUIDE):
        s += "+Guide"
    if p.btn(p.GAMEPAD1_BUTTON_START):
        s += "+Start"
    if p.btn(p.GAMEPAD1_BUTTON_Y):
        s += "+Y"
    if p.btn(p.GAMEPAD1_BUTTON_X):
        s += "+X"
    if p.btn(p.GAMEPAD1_BUTTON_B):
        s += "+B"
    if p.btn(p.GAMEPAD1_BUTTON_A):
        s += "+A"
    if p.btn(p.GAMEPAD1_BUTTON_BACK):
        s += "+Back"
    if p.btn(p.GAMEPAD1_BUTTON_LEFTSHOULDER):
        s += "+L"
    if p.btn(p.GAMEPAD1_BUTTON_RIGHTSHOULDER):
        s += "+R"
    if p.btn(p.GAMEPAD1_BUTTON_DPAD_LEFT):
        s += "+DL"
    if p.btn(p.GAMEPAD1_BUTTON_DPAD_RIGHT):
        s += "+DR"
    if p.btn(p.GAMEPAD1_BUTTON_DPAD_UP):
        s += "+DU"
    if p.btn(p.GAMEPAD1_BUTTON_DPAD_DOWN):
        s += "+DD"

    # on enlève le premier "+"
    if s != "":
        s = s[1:]

    # récupération de la valeur des axes
    axis_leftx = p.btnv(p.GAMEPAD1_AXIS_LEFTX)
    axis_lefty = p.btnv(p.GAMEPAD1_AXIS_LEFTY)
    button_left = p.btn(p.GAMEPAD1_BUTTON_LEFTSTICK)

    axis_rightx = p.btnv(p.GAMEPAD1_AXIS_RIGHTX)
    axis_righty = p.btnv(p.GAMEPAD1_AXIS_RIGHTY)
    button_right = p.btn(p.GAMEPAD1_BUTTON_RIGHTSTICK)
    
    axis_lefttrig = p.btnv(p.GAMEPAD1_AXIS_TRIGGERLEFT)
    axis_righttrig = p.btnv(p.GAMEPAD1_AXIS_TRIGGERRIGHT)


def axis_rect(x, y, w , h, val_x, val_y, button):
    """dessin d'un carré pour un stick
        rectangle vide avec un point qui représente la position du stick
        le point passe au rouge quand le bouton est appuyé"""
    p.rectb(x, y, w+1, h+1, p.COLOR_NAVY)
    if button:
        p.rect(val_x * (w - 2) / 2 / 32768 + x + w/2 - 1, val_y * (h - 2) / 2 / 32768 + y + h/2 - 1, 3, 3, p.COLOR_RED)
    else:
        p.rectb(val_x * (w - 2) / 2 / 32768 + x + w/2 - 1, val_y * (h - 2) / 2 / 32768 + y + h/2 - 1, 3, 3, p.COLOR_CYAN)


def axis_level(x ,y, w, h, val):
    """dessin d'un niveau pour les gachettes
            la hauteur du rectangle est proportionnelle à la valeur de la gachette"""
    p.rectb(x, y, w + 1, h + 1, p.COLOR_NAVY)
    p.rect(x+1, y + h - val/32768*(h-1), w-1, val/32768*(h-1), p.COLOR_CYAN)


def draw():
    """dessin de la fenetre"""
    # repeindre la fenetre en noir
    p.cls(p.COLOR_BLACK)
    # gachette gauche
    p.text(20, 10, 'ZL', p.COLOR_LIME)
    axis_level(20, 24, 5, 80, axis_lefttrig)
    # boutons
    p.text(60, 80, "Buttons", p.COLOR_ORANGE)
    p.text(60, 95, s, p.COLOR_RED)
    # stick gauche
    p.text(50, 10, 'Left', p.COLOR_LIME)
    axis_rect(50, 24, 40, 40, axis_leftx , axis_lefty, button_left)
    # stick droit
    p.text(120, 10, 'Right', p.COLOR_LIME)
    axis_rect(120, 24, 40, 40, axis_rightx, axis_righty, button_right)
    # gachette droite
    p.text(185, 10, 'ZR', p.COLOR_LIME)
    axis_level(185, 24, 5, 80, axis_righttrig)


# lancer le programme
p.run(update, draw)
